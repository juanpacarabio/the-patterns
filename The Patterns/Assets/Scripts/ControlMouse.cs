using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMouse : MonoBehaviour
{

    public static ControlMouse instance = null;
    GameObject touched;
    public bool MousePress;
    GameObject Grid;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }

    void Start()
    {
        Grid = GameObject.Find("Grid");

    }
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            MousePress = true;

        }
        else
        {
            MousePress = false;
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.DrawLine(ray.origin, hit.point);
                touched = hit.transform.gameObject;
                if (touched.CompareTag("GridSpace"))
                {
                    GridSpace script = touched.GetComponent<GridSpace>();
                    script.Select();
                }

            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            GridManager script = Grid.GetComponent<GridManager>();
            script.ExecuteCommand();
        }

    }

}
