using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridArea : MonoBehaviour
{
    public GameObject Grid;
    private void OnMouseExit()
    {
        GridManager script = Grid.GetComponent<GridManager>();
        script.ExecuteCommand();

        foreach (GameObject space in script.spaces)
        {
            GridSpace objectScript = space.GetComponent<GridSpace>();
            objectScript.DeSelect();
        }
    }

}
