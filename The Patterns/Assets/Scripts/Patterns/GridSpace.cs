using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSpace : MonoBehaviour
{
    [SerializeField] bool isSelected;
    public string value;
    public GameObject GridObject;
    GridManager Grid;
    ControlMouse Mouse;
    private void Start()
    {
        Grid = GridObject.GetComponent<GridManager>();
        Mouse = ControlMouse.instance;
        isSelected = false;
        this.GetComponent<Renderer>().material.color = Color.red;
        value = this.gameObject.name;
    }

    private void Update()
    {
        if (Mouse.MousePress == false)
        {
            DeSelect();
        }
    }

    private void OnMouseEnter()
    {
        if (Mouse.MousePress == true && isSelected == false)
        {
            Select();
        }
    }

    public void Select()
    {
        this.GetComponent<Renderer>().material.color = Color.green;
        Grid.sequence = Grid.sequence + value;
        isSelected = true;
    }

    public void DeSelect()
    {
        this.GetComponent<Renderer>().material.color = Color.red;
        isSelected = false;
    }
}
